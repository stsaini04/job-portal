package org.commit.course;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;


@EnableAspectJAutoProxy
@EnableAsync
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
@ComponentScan({"org.commit.course.controller","org.commit.course.dao","org.commit.course.mail"})
@EntityScan(value = "org.commit.course.entity")
public class Jobportal11Application {

	public static void main(String[] args) {
		SpringApplication.run(Jobportal11Application.class, args);
	}

}
