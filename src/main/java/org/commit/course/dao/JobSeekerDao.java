/**
 * 
 */
package org.commit.course.dao;

import java.util.List;

import org.commit.course.entity.JobPostingsView;
import org.commit.course.entity.JobSeeker;


public interface JobSeekerDao {
	
	
	public List<JobPostingsView> filterJobs(JobPostingsView jpv, List<?> jobIds);

	
	public JobSeeker createJobSeeker(JobSeeker job) throws Exception;

	
	public JobSeeker updateJobSeeker(JobSeeker js);

	
	public JobSeeker getJobSeeker(int id);

	
	public List<String> PasswordLookUp(String emailid);

	public void verify(JobSeeker j);
	
	
	public List<?> searchJobs(String searchString);

	public List<Integer> getUserIdFromEmail(String emailid);
}
