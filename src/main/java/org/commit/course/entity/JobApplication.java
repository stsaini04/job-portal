
package org.commit.course.entity;
import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name="jobApplication")
public class JobApplication {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
 int appId;


	@JoinColumn(name = "job_id", nullable = false)
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
 	JobPosting jobposting;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "jobseeker_id", nullable = false)
	private JobSeeker jobSeeker;
	
	boolean resume;
	
	String resumePath;
	
	 int state;
	
	boolean interviewFlag;
	
	 String interviewLocation;
	
	 Date interviewTime;
	
	boolean interviewAccepted;

	 public JobApplication() {

		}

	public JobApplication(int appId, JobPosting jobposting, JobSeeker jobSeeker, boolean resume, String resumePath,
			int state, boolean interviewFlag, String interviewLocation, Date interviewTime, boolean interviewAccepted) {
		super();
		this.appId = appId;
		this.jobposting = jobposting;
		this.jobSeeker = jobSeeker;
		this.resume = resume;
		this.resumePath = resumePath;
		this.state = state;
		this.interviewFlag = interviewFlag;
		this.interviewLocation = interviewLocation;
		this.interviewTime = interviewTime;
		this.interviewAccepted = interviewAccepted;
	}

	public int getAppId() {
		return appId;
	}

	public void setAppId(int appId) {
		this.appId = appId;
	}

	public JobPosting getJobposting() {
		return jobposting;
	}

	public void setJobposting(JobPosting jobposting) {
		this.jobposting = jobposting;
	}

	public JobSeeker getJobSeeker() {
		return jobSeeker;
	}

	public void setJobSeeker(JobSeeker jobSeeker) {
		this.jobSeeker = jobSeeker;
	}

	public boolean isResume() {
		return resume;
	}

	public void setResume(boolean resume) {
		this.resume = resume;
	}

	public String getResumePath() {
		return resumePath;
	}

	public void setResumePath(String resumePath) {
		this.resumePath = resumePath;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public boolean isInterviewFlag() {
		return interviewFlag;
	}

	public void setInterviewFlag(boolean interviewFlag) {
		this.interviewFlag = interviewFlag;
	}

	public String getInterviewLocation() {
		return interviewLocation;
	}

	public void setInterviewLocation(String interviewLocation) {
		this.interviewLocation = interviewLocation;
	}

	public Date getInterviewTime() {
		return interviewTime;
	}

	public void setInterviewTime(Date interviewTime) {
		this.interviewTime = interviewTime;
	}

	public boolean isInterviewAccepted() {
		return interviewAccepted;
	}

	public void setInterviewAccepted(boolean interviewAccepted) {
		this.interviewAccepted = interviewAccepted;
	}
    
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JobApplication other = (JobApplication) obj;
		if (appId != other.appId)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "JobApplication [appId=" + appId + ", jobposting=" + jobposting + ", jobSeeker=" + jobSeeker
				+ ", resume=" + resume + ", resumePath=" + resumePath + ", state=" + state + ", interviewFlag="
				+ interviewFlag + ", interviewLocation=" + interviewLocation + ", interviewTime=" + interviewTime
				+ ", interviewAccepted=" + interviewAccepted + "]";
	}
	 
	 
}