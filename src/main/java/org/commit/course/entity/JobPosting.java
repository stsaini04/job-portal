package org.commit.course.entity;

import javax.persistence.*;

@Entity
@Table(name="jobPosting")
public class JobPosting {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "job_id")
	int jobId;

	public JobSeeker getJobSeeker() {
		return jobSeeker;
	}

	public void setJobSeeker(JobSeeker jobSeeker) {
		this.jobSeeker = jobSeeker;
	}

	@ManyToOne( fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "jobseeker_id",  nullable = false)
	private JobSeeker jobSeeker;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company_id")
	Company company;

	int state;

	String title;

	String description;

	String responsibilities;

	String location;

	 String salary;

	 public JobPosting() {

		}

	public JobPosting(int jobId, Company company, int state, String title, String description, String responsibilities,
			String location, String salary) {
		super();
		this.jobId = jobId;
		this.company = company;
		this.state = state;
		this.title = title;
		this.description = description;
		this.responsibilities = responsibilities;
		this.location = location;
		this.salary = salary;
	}

	public int getJobId() {
		return jobId;
	}

	public void setJobId(int jobId) {
		this.jobId = jobId;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getResponsibilities() {
		return responsibilities;
	}

	public void setResponsibilities(String responsibilities) {
		this.responsibilities = responsibilities;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getSalary() {
		return salary;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JobPosting other = (JobPosting) obj;
		if (jobId != other.jobId)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "JobPosting [jobId=" + jobId + ", company=" + company + ", state=" + state + ", title=" + title
				+ ", description=" + description + ", responsibilities=" + responsibilities + ", location=" + location
				+ ", salary=" + salary + "]";
	}

	public void setKeywords(String string) {
		// TODO Auto-generated method stub
		
	}

	 
	

}