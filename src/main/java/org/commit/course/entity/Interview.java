package org.commit.course.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Interview {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	int ID;
	
	 String company;
	
	 int jobseekerid;
	
      String location;
	
	 String datetime;
	
	 String flag;
	
	 public Interview() {

		}

	public Interview(int iD, String company, int jobseekerid, String location, String datetime, String flag) {
		super();
		ID = iD;
		this.company = company;
		this.jobseekerid = jobseekerid;
		this.location = location;
		this.datetime = datetime;
		this.flag = flag;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public int getJobseekerid() {
		return jobseekerid;
	}

	public void setJobseekerid(int jobseekerid) {
		this.jobseekerid = jobseekerid;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Interview other = (Interview) obj;
		if (ID != other.ID)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Interview [ID=" + ID + ", company=" + company + ", jobseekerid=" + jobseekerid + ", location="
				+ location + ", datetime=" + datetime + ", flag=" + flag + "]";
	}
	

}
