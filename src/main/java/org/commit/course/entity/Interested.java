package org.commit.course.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Interested{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	int ID;
	
 int jobId;
	int jobSeekerId;
	
	public Interested() {

	}

	public Interested(int iD, int jobId, int jobSeekerId) {
		super();
		ID = iD;
		this.jobId = jobId;
		this.jobSeekerId = jobSeekerId;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public int getJobId() {
		return jobId;
	}

	public void setJobId(int jobId) {
		this.jobId = jobId;
	}

	public int getJobSeekerId() {
		return jobSeekerId;
	}

	public void setJobSeekerId(int jobSeekerId) {
		this.jobSeekerId = jobSeekerId;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Interested other = (Interested) obj;
		if (ID != other.ID)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Interested [ID=" + ID + ", jobId=" + jobId + ", jobSeekerId=" + jobSeekerId + "]";
	}

	
	
	
	
	
}
