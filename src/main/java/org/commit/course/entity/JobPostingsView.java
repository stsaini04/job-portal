
package org.commit.course.entity;

import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Immutable
public class JobPostingsView {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	int jobId ;
	String title;
	String description;
	String responsibilities;
	String location;
	String salary;
	int companyId;
	String companyName;
	String keywords;
	int state;
	 public JobPostingsView() {

		}
	 
	 
	public JobPostingsView(int jobId, String title, String description, String responsibilities, String location,
			String salary, int companyId, String companyName, String keywords, int state) {
		super();
		this.jobId = jobId;
		this.title = title;
		this.description = description;
		this.responsibilities = responsibilities;
		this.location = location;
		this.salary = salary;
		this.companyId = companyId;
		this.companyName = companyName;
		this.keywords = keywords;
		this.state = state;
	}


	public int getJobId() {
		return jobId;
	}


	public void setJobId(int jobId) {
		this.jobId = jobId;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getResponsibilities() {
		return responsibilities;
	}


	public void setResponsibilities(String responsibilities) {
		this.responsibilities = responsibilities;
	}


	public String getLocation() {
		return location;
	}


	public void setLocation(String location) {
		this.location = location;
	}


	public String getSalary() {
		return salary;
	}


	public void setSalary(String salary) {
		this.salary = salary;
	}


	public int getCompanyId() {
		return companyId;
	}


	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}


	public String getCompanyName() {
		return companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public String getKeywords() {
		return keywords;
	}


	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}


	public int getState() {
		return state;
	}


	public void setState(int state) {
		this.state = state;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JobPostingsView other = (JobPostingsView) obj;
		if (jobId != other.jobId)
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "JobPostingsView [jobId=" + jobId + ", title=" + title + ", description=" + description
				+ ", responsibilities=" + responsibilities + ", location=" + location + ", salary=" + salary
				+ ", companyId=" + companyId + ", companyName=" + companyName + ", keywords=" + keywords + ", state="
				+ state + "]";
	}

}
