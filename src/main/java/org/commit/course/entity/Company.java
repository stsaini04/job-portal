
package org.commit.course.entity;

import javax.persistence.*;

@Entity
public class Company {
@Id
@GeneratedValue(strategy = GenerationType.SEQUENCE)
@Column(name = "company_id")
 int companyId;
 String companyName;
	 String headquarters;
	
 String companyUser;
	
String password;
	
	 String description;

	 boolean verified;
	int verificationCode;

	public Company() {

	}

	public Company(int companyId, String companyName, String headquarters, String companyUser, String password,
			String description, boolean verified, int verificationCode) {
		super();
		this.companyId = companyId;
		this.companyName = companyName;
		this.headquarters = headquarters;
		this.companyUser = companyUser;
		this.password = password;
		this.description = description;
		this.verified = verified;
		this.verificationCode = verificationCode;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getHeadquarters() {
		return headquarters;
	}

	public void setHeadquarters(String headquarters) {
		this.headquarters = headquarters;
	}

	public String getCompanyUser() {
		return companyUser;
	}

	public void setCompanyUser(String companyUser) {
		this.companyUser = companyUser;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	public int getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(int verificationCode) {
		this.verificationCode = verificationCode;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Company other = (Company) obj;
		if (companyId != other.companyId)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Company [companyId=" + companyId + ", companyName=" + companyName + ", headquarters=" + headquarters
				+ ", companyUser=" + companyUser + ", password=" + password + ", description=" + description
				+ ", verified=" + verified + ", verificationCode=" + verificationCode + "]";
	}
	
	
	
}