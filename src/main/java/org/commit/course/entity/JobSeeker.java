
package org.commit.course.entity;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "jobSeeker")
public class JobSeeker {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "jobseeker_id")
	int jobseekerId;

	String firstName;
String lastName;

	String emailId;

	String password;

	int workEx;

	int highestEducation;

	String skills;

	boolean verified;

	int verificationCode;

	@OneToMany(fetch = FetchType.LAZY, mappedBy="jobSeeker")
	private List<JobPosting> interestedjobs;

	@OneToMany(fetch = FetchType.LAZY, mappedBy="jobSeeker")
	private List<JobApplication> jobApplicationList;
	 public JobSeeker() {

		}
	public JobSeeker(int jobseekerId, String firstName, String lastName, String emailId, String password, int workEx,
			int highestEducation, String skills, boolean verified, int verificationCode,
			List<JobPosting> interestedjobs, List<JobApplication> jobApplicationList) {
		super();
		this.jobseekerId = jobseekerId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.emailId = emailId;
		this.password = password;
		this.workEx = workEx;
		this.highestEducation = highestEducation;
		this.skills = skills;
		this.verified = verified;
		this.verificationCode = verificationCode;
		this.interestedjobs = interestedjobs;
		this.jobApplicationList = jobApplicationList;
	}

	public int getJobseekerId() {
		return jobseekerId;
	}

	public void setJobseekerId(int jobseekerId) {
		this.jobseekerId = jobseekerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getWorkEx() {
		return workEx;
	}

	public void setWorkEx(int workEx) {
		this.workEx = workEx;
	}

	public int getHighestEducation() {
		return highestEducation;
	}

	public void setHighestEducation(int highestEducation) {
		this.highestEducation = highestEducation;
	}

	public String getSkills() {
		return skills;
	}

	public void setSkills(String skills) {
		this.skills = skills;
	}

	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	public int getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(int verificationCode) {
		this.verificationCode = verificationCode;
	}

	public List<JobPosting> getInterestedjobs() {
		return interestedjobs;
	}

	public void setInterestedjobs(List<JobPosting> interestedjobs) {
		this.interestedjobs = interestedjobs;
	}

	public List<JobApplication> getJobApplicationList() {
		return jobApplicationList;
	}

	public void setJobApplicationList(List<JobApplication> jobApplicationList) {
		this.jobApplicationList = jobApplicationList;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JobSeeker other = (JobSeeker) obj;
		if (jobseekerId != other.jobseekerId)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "JobSeeker [jobseekerId=" + jobseekerId + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", emailId=" + emailId + ", password=" + password + ", workEx=" + workEx + ", highestEducation="
				+ highestEducation + ", skills=" + skills + ", verified=" + verified + ", verificationCode="
				+ verificationCode + ", interestedjobs=" + interestedjobs + ", jobApplicationList=" + jobApplicationList
				+ "]";
	}



}
